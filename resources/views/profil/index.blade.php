@extends('layouts.master')
@section('judul')
    Halaman Update Profile
@endsection

@section('konten')
<form action="/profil/{{ $profil->id }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')

    <div class="form-group">
        <label>Email</label>
        <input type="email" class="form-control" id="email" value="{{ $profil->user->email }}" disabled>
    </div>

    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" id="name" value="{{ $profil->user->name }}" disabled>
    </div> 


    <div class="form-group">
        <label for="tanggal_lahir">Tanggal Lahir</label>
        <input type="date" class="form-control" name="tanggal_lahir" id="tanggal_lahir" value="{{ $profil->tanggal_lahir }}" placeholder="Masukkan Tanggal Lahir">
        @error('tanggal_lahir')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label for="alamat">Alamat</label>
        <input type="text" class="form-control" name="alamat" id="alamat" value="{{ $profil->alamat }}" placeholder="Masukkan Alamat">
        @error('alamat')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label for="motto">Motto</label>
        <input type="text" class="form-control" name="motto" id="motto" value="{{ $profil->motto }}" placeholder="Masukkan Motto">
        @error('motto')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

     
    
     
    <button type="submit" class="btn btn-primary">Edit</button>
</form>
@endsection