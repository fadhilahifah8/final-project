@extends('layouts.master')
@section('judul')
    Edit Film : {{ $buku->judul }}
@endsection

@push('scripts')
<script src="https://cdn.tiny.cloud/1/xld6mvbthu0ku6quw3optwb015q22vg7l0asvakjdgq92mwl/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste advtable tinymcespellchecker',
      //toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      //tinycomments_mode: 'embedded',
      //tinycomments_author: 'Author name',
   });
  </script>
@endpush

@section('konten')
<form action="/buku/{{ $buku->id }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="judul">Judul</label>
        <input type="text" class="form-control" name="judul" id="judul" value="{{ $buku->judul }}" placeholder="Masukkan judul">
        @error('judul')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="isbn">ISBN</label>
        <input type="number" class="form-control" name="isbn" id="isbn" value="{{ $buku->isbn }}" placeholder="Masukkan nomor ISBN">
        @error('isbn')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="kategori_id">Kategori</label>
        <select class="form-control" name="kategori_id" id="kategori_id">
            <option value="">-- Pilih Kategori -- </option>
            @foreach ($kategori as $item)
                @if ($item->id === $buku->kategori_id)
                    <option value="{{ $item->id }}" selected>{{ $item->nama }}</option>
                @else
                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                @endif
            @endforeach
        </select>
        @error('kategori_id')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="penulis">Penulis</label>
        <input type="text" class="form-control" name="penulis" id="penulis" value="{{ $buku->penulis }}" placeholder="Masukkan penulis">
        @error('penulis')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="penerbit">Penerbit</label>
        <input type="text" class="form-control" name="penerbit" id="penerbit" value="{{ $buku->penerbit }}" placeholder="Masukkan penerbit">
        @error('penerbit')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="tahun">Tahun</label>
        <input type="number" class="form-control" name="tahun" id="tahun" value="{{ $buku->tahun }}" placeholder="Masukkan tahun">
        @error('tahun')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="gambar">Cover Buku</label>
        <input type="file" class="form-control" name="gambar" id="gambar">
        @error('gambar')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="file">File Buku (PDF)</label>
        <input type="file" class="form-control" name="file" id="file">
        @error('file')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="ringkasan">Ringkasan</label>
        <textarea class="form-control" name="ringkasan" id="ringkasan" cols="30" rows="10" placeholder="Masukkan ringkasan">{{ $buku->ringkasan }}</textarea>
        @error('ringkasan')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
       
    <button type="submit" class="btn btn-primary">Edit</button>
</form>
@endsection