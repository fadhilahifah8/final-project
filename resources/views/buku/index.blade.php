@extends('layouts.master')
@section('judul')
    List Buku
@endsection

@section('konten')

@auth
    <a href="/buku/create" class="btn btn-success my-2">Tambah Buku</a>  
@endauth

<div class="row">
    @foreach ($buku as $item)    
    <!-- membuat card menjadi 3 colom (max 12/3 = col-4) -->
    <div class="col-4">
        <div class="card">
            <img src="{{ asset('img/'.$item->gambar) }}" class="card-img-top rounded" width="auto" 
            height="300" alt="...">
            <div class="card-body">
                <h5 class="card-title"><b>{{ $item->judul }} ({{ $item->tahun }})</b></h5>
                @auth
                    <form action="/buku/{{ $item->id }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <a href="/buku/{{ $item->id }}" class="btn btn-primary">Read More</a>
                        <a href="/buku/{{ $item->id }}/edit" class="btn btn-info">Edit</a>
                        <input type="submit" class="btn btn-danger" value="Delete">
                    </form>
                @endauth

                @guest
                    <a href="/buku/{{ $item->id }}" class="btn btn-primary">Read More</a>
                @endguest
            </div>
        </div>
    </div>
    @endforeach
</div>


@endsection