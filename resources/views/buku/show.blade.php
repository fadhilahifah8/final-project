@extends('layouts.master')
@section('judul')
    Detail Buku : {{ $buku->judul }}
@endsection

@push('scripts')
<script src="https://cdn.tiny.cloud/1/xld6mvbthu0ku6quw3optwb015q22vg7l0asvakjdgq92mwl/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste advtable tinymcespellchecker',
      //toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      //tinycomments_mode: 'embedded',
      //tinycomments_author: 'Author name',
   });
  </script>
@endpush

@section('konten')

    <div class="card mb-3">
        <div class="row no-gutters">
            <div class="col-md-3">
                <img src="{{ asset('img/'.$buku->gambar) }}"  alt="...">
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <div class="container">
                        <h3 class="card-title font-weight-bolder">{{ $buku->judul }}</h3>
                        <i class="card-text text-primary">By : {{ $buku->penulis }}</i>
                        <p class="card-text text-dark">{{ $buku->ringkasan }}</p>
                    <div class="row">
                        <div class="col-md-2">Kategori:</div>
                        <div class="col text-dark">{{ $buku->kategori->nama }}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">No ISBN:</div>
                        <div class="col text-dark">{{ $buku->isbn }}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">Penerbit:</div>
                        <div class="col text-dark">{{ $buku->penerbit }}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">Tahun:</div>
                        <div class="col text-dark">{{ $buku->tahun }}</div>
                    </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <a href="{{ asset('file/'.$buku->file) }}" target="_blank" class="btn btn-info">
            <i class="fa fa-download"></i> Download File
        </a>
    </div>
    
    {{-- comments form --}}
    <div class="card my-4">
        <h5 class="card-header bg-light text-dark">Berikan Ulasan</h5>
        <div class="card-body">
            <form action="/buku/{{ $buku->id }}" method="POST">
                @csrf
                <div class="form-group">
                    <textarea class="form-control" name="isi" id="isi" cols="30" rows="10" placeholder="Tuliskan ulasan"></textarea>
                    @error('isi')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Tambah</button>
                
                @guest
                    <p><i>*Login terlebih dahulu untuk memberikan ulasan </i></p>
                @endguest
            </form>
        </div>    
    </div>

    {{-- single comment --}}
    @foreach ($ulasan as $item)
    @if ($item->buku_id==$buku->id)
        <div class="media mb-4 bg-white rounded">
            <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="...">
            <div class="media-body">
                <h5 class="mt-0 text-primary" >{{ $item->user->name}}</h5>
                <p>{!! $item->isi !!}</p>
            </div>
        </div>
    @endif
        
    @endforeach
        
    

@endsection