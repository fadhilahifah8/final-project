<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buku;
use App\Ulasan;
use App\Kategori;
use File;
use Auth;
use RealRashid\SweetAlert\Facades\Alert;

class BukuController extends Controller
{
    public function __construct()
    {

        $this->middleware('auth')->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buku = Buku::all();
        return view('buku.index', compact('buku'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::all();
        return view('buku.create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'judul' => 'required',
    		'isbn' => 'required',
    		'penulis' => 'required',
    		'penerbit' => 'required',
    		'tahun' => 'required',
    		'gambar' => 'required|mimes:jpeg,jpg,png|max:2200',
    		'file' => 'required|mimes:pdf',
    		'ringkasan' => 'required',
    		'kategori_id' => 'required'
    	]);
        
        $gambar = $request->gambar; 
        $new_gambar = time().'-'.$gambar->getClientOriginalName();

        $file = $request->file;
        $new_file = time().'-'.$file->getClientOriginalName();

        Buku::create([
    		'judul' => $request->judul,
    		'isbn' => $request->isbn,
    		'penulis' => $request->penulis,
    		'penerbit' => $request->penerbit,
    		'tahun' => $request->tahun,
    		'gambar' => $new_gambar,
    		'file' => $new_file,
    		'ringkasan' => $request->ringkasan,
    		'kategori_id' =>$request->kategori_id
    	]);
        $gambar->move('img/', $new_gambar);
        $file->move('file/', $new_file);
        Alert::success('Berhasil', 'Berhasil tambah buku');
        return redirect('/buku');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $buku = Buku::find($id);
        $ulasan = Ulasan::all();
        return view('buku.show', compact('buku', 'ulasan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $buku = Buku::find($id);
        $kategori = Kategori::all();
        return view('buku.edit', compact('buku', 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'judul' => 'required',
    		'isbn' => 'required',
    		'penulis' => 'required',
    		'penerbit' => 'required',
    		'tahun' => 'required',
    		'gambar' => 'mimes:jpeg,jpg,png|max:2200',
    		'file' => 'mimes:pdf',
    		'ringkasan' => 'required',
    		'kategori_id' => 'required'
    	]);

        $buku = Buku::findorfail($id);

        if ($request->has('gambar')) { 
            $path = 'img/'; 
            File::delete($path . $buku->gambar); 
            $gambar = $request->gambar; 
            $new_gambar = time().'-'.$gambar->getClientOriginalName(); 
            $gambar->move($path, $new_gambar); 
            $buku_data = [
                'judul' => $request->judul,
                'isbn' => $request->isbn,
                'penulis' => $request->penulis,
                'penerbit' => $request->penerbit,
                'tahun' => $request->tahun,
                'gambar' => $new_gambar,
                'ringkasan' => $request->ringkasan,
                'kategori_id' =>$request->kategori_id
            ];
        } elseif ($request->has('file')) {
            $path = 'file/';
            File::delete($path . $buku->file);
            $file = $request->file; 
            $new_file = time().'-'.$file->getClientOriginalName(); 
            $file->move($path, $new_file); 
            $buku_data = [
                'judul' => $request->judul,
                'isbn' => $request->isbn,
                'penulis' => $request->penulis,
                'penerbit' => $request->penerbit,
                'tahun' => $request->tahun,
                'file' => $new_file,
                'ringkasan' => $request->ringkasan,
                'kategori_id' =>$request->kategori_id
            ];
        } else {
            $buku_data = [
                'judul' => $request->judul,
                'isbn' => $request->isbn,
                'penulis' => $request->penulis,
                'penerbit' => $request->penerbit,
                'tahun' => $request->tahun,
                'ringkasan' => $request->ringkasan,
                'kategori_id' =>$request->kategori_id
            ];
        }

        $buku->update($buku_data);
        Alert::success('Berhasil', 'Berhasil Edit buku');
        return redirect('/buku');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $buku = Buku::findorfail($id);
        $buku->delete();

        $path = 'img/';
        $path_file = 'file/';
        File::delete($path . $buku->gambar);
        File::delete($path_file . $buku->file);
        Alert::success('Berhasil', 'Berhasil hapus buku');
        return redirect('/buku');
    }
    
    public function ulasan(Request $request, $id)
    {   
        $this->validate($request,[
    		'isi' => 'required'
    	]);

        $buku = Buku::find($id);
        // $user = User::find($id);
        Ulasan::create([
    		'isi' => $request->isi,
    		'buku_id' => $buku->id, 
    		'user_id' => Auth::user()->id
    	]);
        
        return redirect()->action('BukuController@show', $id);
        
    }
}
