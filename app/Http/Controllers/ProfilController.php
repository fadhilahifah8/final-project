<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profil;
use Auth;
use RealRashid\SweetAlert\Facades\Alert;

class ProfilController extends Controller
{
    public function index() {
        $profil = Profil::where('user_id', Auth::user()->id)->first();
        return view('profil.index', compact('profil'));

    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'tanggal_lahir' => 'required',
            'alamat' => 'required',
            'motto' => 'required',
        ]);

        $profil_data = [
            'tanggal_lahir' => $request->tanggal_lahir,
            'alamat' =>  $request->alamat, 
            'motto' =>  $request->motto,
        ];

        Profil::whereId($id)->update($profil_data);
        Alert::success('Berhasil', 'Berhasil update profil');
        return redirect('profil');
    }
}
