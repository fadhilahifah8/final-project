<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = 'buku';
    protected $fillable = ['judul', 'isbn', 'penulis', 'penerbit', 'tahun', 'gambar', 'file', 'ringkasan', 'kategori_id'];

    public function kategori()
    {
        return $this->belongsTo('App\Kategori');
    }

    public function ulasan()
    {
        return $this->hasMany('App\Ulasan');
    }

}
