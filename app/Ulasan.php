<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ulasan extends Model
{
    protected $table = 'ulasan';
    protected $fillable = ['user_id', 'buku_id', 'isi'];

    public function buku()
    {
        return $this->belongsTo('App\Buku'); 
    }

    public function user()
    {
        return $this->belongsTo('App\User'); 
    }
}
