<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use RealRashid\SweetAlert\Facades\Alert;


Route::group(['middleware' => ['auth']], function () {
    Route::get('/', function () {
        Alert::success('Sukses', 'Selamat Datang');
        return view('welcome');
    });

    //crud kategori
    Route::resource('kategori', 'KategoriController');
});


Route::resource('buku', 'BukuController');

Route::post('/buku/{buku_id}', 'BukuController@ulasan');

Route::resource('profil', 'ProfilController')->only([
    'index', 'update',
]);

Auth::routes();
